package xml;

import java.io.*;
import java.util.Iterator;

import static xml.XmlPartReaderImpl2.TagPosition.BEGIN;
import static xml.XmlPartReaderImpl2.TagPosition.END;

public class XmlPartReaderImpl2 implements XmlPartReader {

    private final InputStream inputReader;
    private final char[] tagChars;
    private char currentChar = 0;

    private final String startTag;
    private boolean startTagFound = false;
    private StringWriter writer;

    public XmlPartReaderImpl2(String tagName, InputStream inputStream, long startPosition) throws IOException {
        this.startTag = "<"+tagName+">";

        this.tagChars = tagName.toCharArray();
        long skipped = inputStream.skip(startPosition);
        System.out.println("skipped: "+skipped);
        this.inputReader = new BufferedInputStream(inputStream, 1024 * 640);
    }

    @Override
    public void close() throws IOException {
        inputReader.close();
    }

    @Override
    public Iterator<String> iterator() {
        return new XmlPartIterator(this);
    }

    public boolean hasNext() throws IOException {
        return startTagFound || positionBeforeNextTag();
    }

    public String getNextXmlPart() throws IOException {
        if(hasNext()) {
            return readUntilEndTag();
        }
        else return null;
    }

    private String readUntilEndTag() throws IOException {
        writer = new StringWriter();
        writer.write(startTag);
        readUntilTag(END);
        String part = writer.toString();
        resetForNextTag();
        return part;
    }

    private void resetForNextTag() {
        startTagFound = false;
        writer = null;
    }

    private boolean positionBeforeNextTag() throws IOException {
        startTagFound = readUntilTag(BEGIN);
        return startTagFound;
    }

    enum TagPosition {
        BEGIN,
        END;
    }

    private boolean readUntilTag(TagPosition position) throws IOException {
        boolean nextTagFound = false;
        try{
            while (!nextTagFound) {
                readNextChar();
                if(currentChar == '<') {
                    readNextChar();
                    if(isStartOfEndTag(position) || isStartOfTag(position)) {
                        nextTagFound = isFullTagRead();
                    }
                }
            }
        } catch (EOFException e) {
        }
        return nextTagFound;
    }

    private boolean isStartOfTag(TagPosition position) {
        return position == BEGIN && currentChar != '/';
    }

    private boolean isStartOfEndTag(TagPosition position) throws IOException {
        return position == END && currentChar == '/' && readNextChar();
    }

    private boolean isFullTagRead() throws IOException {
        int index = 0;
        while(currentChar == tagChars[index] && index < tagChars.length-1) {
            readNextChar();
            index++;
        }
        if(index == tagChars.length-1) {
            readNextChar();
            return currentChar == '>';
        }
        else return false;
    }

    private boolean readNextChar() throws IOException {
        int b = inputReader.read();
        if(b == -1) throw new EOFException();
        if(writer != null) writer.write(b);
        currentChar = (char)b;
        return true;
    }
}
