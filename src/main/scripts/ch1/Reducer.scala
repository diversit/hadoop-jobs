#!/bin/sh
  exec scala "$0" "$@"
!#

val Input = """(\d+)\t([+-]\d+)""".r
var max = Int.MinValue
var lastKey: String = _

def out(key: String, temp: Int) = println("%s\t%s" format(key, temp))

for (ln <- io.Source.stdin.getLines) {

  def parse(s: String) = if (s.startsWith("+")) s.substring(1).toInt else s.toInt

  ln match {
    case Input(k, v) => {
      val temp = parse(v)

      max = if (lastKey != null && lastKey != k) {
        out(lastKey, max)
        temp
      } else {
        if (temp > max) temp else max
      }
      lastKey = k
    }
    case _ =>
  }
}
out(lastKey, max)


