package util;

public interface Function2<A, B> {
    B apply(B b, A a);
}
