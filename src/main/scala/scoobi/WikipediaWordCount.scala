package scoobi

import wikipedia.WikipediaXmlInputFormat
import com.nicta.scoobi.{DList, ScoobiApp}
import org.apache.hadoop.io.Text
import org.apache.hadoop.fs.Path
import com.nicta.scoobi.io.{Helper, DataSource, InputConverter}
import java.io.IOException
import org.apache.hadoop.mapreduce.Job
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat
import org.apache.commons.logging.LogFactory

object XmlInput {
  lazy val logger = LogFactory.getLog("scoobi.TextInput")

  def fromXmlFile(path: String): DList[String] = {
    val converter = new InputConverter[Text, Text, String] {
      def fromKeyValue(context: this.type#InputContext, key: Text, value: Text) = {
        value.toString
      }
    }

    DList.fromSource(new XmlSource(List(path), converter))
  }

  class XmlSource(paths: List[String], converter: InputConverter[Text, Text, String]) extends DataSource[Text, Text, String] {

    private val inputPaths = paths.map(p => new Path(p))

    val inputFormat = classOf[WikipediaXmlInputFormat]

    def inputCheck() = inputPaths foreach { p =>
      if (Helper.pathExists(p))
        logger.info("Input path: " + p.toUri.toASCIIString + " (" + Helper.sizeString(Helper.pathSize(p)) + ")")
      else
        throw new IOException("Input path " + p + " does not exist.")
    }

    def inputConfigure(job: Job) = inputPaths foreach { p => FileInputFormat.addInputPath(job, p) }

    def inputSize(): Long = inputPaths.map(p => Helper.pathSize(p)).sum

    val inputConverter = converter
  }
}

class WikipediaWordCount extends ScoobiApp {

  val (input, output) =
    if (args.length != 2) {
      sys.error("Usage: WikipediaWordCount [hadoop args] <input> <output>")
      sys.exit(1)
    } else {
      (args(0), args(1))
    }

  import XmlInput._
  val xml: DList[String] = fromXmlFile(input)
}
