package scoobi

import org.scalatest.matchers.ShouldMatchers
import scala.xml.{Elem, XML}
import com.nicta.scoobi.Scoobi._
import java.io.File
import org.scalatest.{BeforeAndAfter, FeatureSpec}
import org.apache.commons.io.FileUtils
import com.nicta.scoobi.WireFormat

object XmlInputTest {
  val targetFolder = new File("output-wiki")
  val wordExpr = """\b[a-zA-Z]+\b""".r

  case class WordCount(val word: String, val count: Int, val weight: Int)

  implicit val wordCountFmt = WireFormat.mkCaseWireFormat(WordCount, WordCount.unapply _)

}

class XmlInputTest extends FeatureSpec with ShouldMatchers with BeforeAndAfter {
  import XmlInputTest._

  before {
    if(targetFolder.exists) FileUtils.deleteDirectory(targetFolder)
  }


  feature("load xml from file") {
    scenario("Count words using custom WireFormat") {
      val list: DList[String] = XmlInput.fromXmlFile("src/main/resources/nlwiki-latest-pages-articles-short.xml")

      def zipWithFactor[A](words: Iterator[A], factor: Int): Iterable[(A, Int)] = words.toIterable zip Stream.continually(factor)

      def getWords(in: String) : Iterable[(String, Int)] = {
        val xml: Elem = XML.loadString(in)
        val title = xml \\ "title" text
        val txt = xml \\ "text" text
        val titleWords = wordExpr findAllIn(title)
        val textWords   = wordExpr findAllIn(txt)
        zipWithFactor(titleWords, 100) ++ zipWithFactor(textWords, 1)
      }

      val mapped: DList[WordCount] = list flatMap (getWords) map { case (w, f) => WordCount(w, 1, f) }

      val grouped: DList[(String, Iterable[WordCount])] = mapped groupBy { wc => wc.word }

      def sumCounts(counts: Iterable[WordCount]) : Int = counts map { wc => wc.count * wc.weight } sum

      val counted: DList[(String)] = grouped.map { case (word, wordCounts) => "%d\t%s" format (sumCounts(wordCounts), word) }

      val out = targetFolder.toURI.toString
      DList.persist(toTextFile(counted, out))
    }


    scenario("load file and read through content") {
      val list: DList[String] = XmlInput.fromXmlFile("src/main/resources/nlwiki-latest-pages-articles-short.xml")

      def getWords(in: String) : Iterable[String] = {
        val xml: Elem = XML.loadString(in)
        val title = xml \\ "title" text
        val txt = xml \\ "text" text
        val titleWords = wordExpr findAllIn(title)
        val textWords   = wordExpr findAllIn(txt)
        titleWords ++ textWords toIterable
      }

      def mapWords(word: String) : (String, Int) = (word, 1)

      val keyValues: DList[(String, Int)] = list flatMap (getWords) map (mapWords)

      val grouped: DList[(String, Iterable[Int])] = keyValues groupByKey

      val combined: DList[(String, Int)] = grouped.combine(_ + _)

      def swap(in: (String, Int)): (Int, String) = in._2 -> in._1
      val combinedSwapped: DList[(Int, String)] = combined map swap

      // create a File so it's relative to working directory
      val out = targetFolder.toURI.toString
      DList.persist(toTextFile(combinedSwapped, out))
    }
  }
}

class WikipediaWordCountTest extends FeatureSpec {

}
