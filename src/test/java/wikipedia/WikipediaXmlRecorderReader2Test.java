package wikipedia;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordReader;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.internal.matchers.TypeSafeMatcher;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class WikipediaXmlRecorderReader2Test extends WikipediaXmlRecorderReaderTests {

    @Override
    public RecordReader<Text, Text> createRecordReader() {
        return new WikipediaXmlRecorderReader2();
    }
}
