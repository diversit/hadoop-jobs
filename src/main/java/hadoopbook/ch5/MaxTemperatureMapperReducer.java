package hadoopbook.ch5;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import util.Collections3;
import util.Function2;

import java.io.IOException;

public class MaxTemperatureMapperReducer extends MapperReducer<LongWritable, Text, Text, IntWritable, Text, IntWritable> {

    public static final Function2<IntWritable,Integer> MAX = new Function2<IntWritable, Integer>() {
        @Override
        public Integer apply(Integer max, IntWritable value) {
            return Math.max(max, value.get());
        }
    };

    @Override
    public void map1(LongWritable key, Text value, Mapper.Context context) throws IOException, InterruptedException {

        TemperatureParser parser = new TemperatureParser(value.toString());
        if(!parser.isTemperatureMissing()) {
            context.write(new Text(parser.getYear()), new IntWritable(parser.getTemperature()));
        }
    }

    @Override
    public void reduce1(Text key, Iterable<IntWritable> values, Reducer.Context context) throws IOException, InterruptedException {

        Integer max = Collections3.foldLeft(values, Integer.MIN_VALUE, MAX);
        context.write(key, new IntWritable(max));
    }
}
