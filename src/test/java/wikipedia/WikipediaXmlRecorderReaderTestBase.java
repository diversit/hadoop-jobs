package wikipedia;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import static wikipedia.WikipediaXmlRecorderReader.XML_READER_TAG;

public class WikipediaXmlRecorderReaderTestBase {

    public static final String TITLE_TAG = "title";
    public static final String XML_FILE = "src/main/resources/nlwiki-latest-pages-articles-short.xml";
    public FileSplit fileSplit;
    public TaskAttemptContext context;

    public void setupRecordReaderParams(long start, long length, String tag) throws Exception {
        fileSplit = new FileSplit(new Path(XML_FILE), start, length, null);
        Configuration config = new Configuration();
        if(tag != null) {
            config.set(XML_READER_TAG, "title");
        }

        context = new TaskAttemptContext(config, new TaskAttemptID());
    }
}
