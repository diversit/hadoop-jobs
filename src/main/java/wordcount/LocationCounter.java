package wordcount;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class LocationCounter implements WritableComparable<LocationCounter> {

    private Location location;
    private Integer count;

    public LocationCounter(final Location location, final Integer count) {
        set(location, count);
    }

    public LocationCounter() {}

    public void set(final Location location, final Integer count) {
        this.location = location;
        this.count = count;
    }

    @Override
    public int compareTo(LocationCounter other) {
        int result = compareLocations(this.location.serialize(), other.location.serialize());
        if(result != 0) return result;

        result = compareCount(this.count, other.count);
        return result;
    }

    private static int compareCount(int selfCount, int otherCount) {
        return selfCount < otherCount ? -1 : selfCount == otherCount ? 0 : 1;
    }

    private static int compareLocations(byte selfLocation, byte otherLocation) {
        return selfLocation < otherLocation ? -1 : selfLocation == otherLocation ? 0 : 1;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeByte(location.serialize());
        dataOutput.writeInt(count);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        location = Location.deserialize(dataInput.readByte());
        count = dataInput.readInt();
    }

    public Location getLocation() {
        return location;
    }

    public Integer getCount() {
        return count;
    }

    /**
     * {@link org.apache.hadoop.io.RawComparator} implementation for this custom {@link org.apache.hadoop.io.Writable}
     */
    public static class Comparator extends WritableComparator {

        public Comparator() {
            super(LocationCounter.class);
        }

        @Override
        public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
            byte bLc1 = b1[0];
            byte bLc2 = b2[0];

            int result = compareLocations(bLc1, bLc2);
            if(result != 0) return result;

            int countLc1 = readInt(b1, s1+1);
            int countLc2 = readInt(b2, s2+1);
            result = compareCount(countLc1, countLc2);

            return result;
        }
    }

    // Register WritableComparator for LocationCounter
    static {
        WritableComparator.define(LocationCounter.class, new Comparator());
    }
}
