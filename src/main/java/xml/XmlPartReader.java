package xml;

import java.io.Closeable;
import java.io.IOException;

public interface XmlPartReader extends Iterable<String>, Closeable {

    boolean hasNext() throws IOException;

    String getNextXmlPart() throws IOException;
}
