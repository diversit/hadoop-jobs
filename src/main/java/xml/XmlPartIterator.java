package xml;

import java.io.IOException;
import java.util.Iterator;

public class XmlPartIterator implements Iterator<String> {

    private XmlPartReader reader;

    public XmlPartIterator(XmlPartReader reader) {
        this.reader = reader;
    }

    @Override
    public boolean hasNext() {
        try {
            return reader.hasNext();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String next() {
        try {
            return reader.getNextXmlPart();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove() {
        throw new NoSuchMethodError("Remove it not implemented for XmlPartIterator");
    }
}
