package wikipedia;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.thirdparty.guava.common.collect.Maps;
import wordcount.Location;
import wordcount.LocationCounter;

import java.io.IOException;
import java.util.Map;

public class WordCounterReducer2 extends Reducer<Text, LocationCounter, IntWritable, Text> {

    public static final int TEXT_WEIGHT = 1;
    public static final int SUMMARY_WEIGHT = 10;
    public static final int TITLE_WEIGHT = 100;

    @Override
    protected void reduce(Text key, Iterable<LocationCounter> values, Context context) throws IOException, InterruptedException {
        Map<Location, Integer> countMap = createNewCountMap();

        for(LocationCounter lc : values) {
            incrementCountForLocation(countMap, lc);
        }

        context.write(calculate(countMap), key);
    }

    private IntWritable calculate(Map<Location, Integer> countMap) {
        int value = TEXT_WEIGHT * countMap.get(Location.TEXT);
        value = value + (SUMMARY_WEIGHT * countMap.get(Location.SUMMARY));
        value = value + (TITLE_WEIGHT * countMap.get(Location.TITLE));
        return new IntWritable(value);
    }

    private void incrementCountForLocation(Map<Location, Integer> countMap, LocationCounter lc) {
        Integer count = countMap.get(lc.getLocation());
        countMap.put(lc.getLocation(), count + lc.getCount());
    }

    private Map<Location, Integer> createNewCountMap() {
        Map<Location, Integer> countMap = Maps.newHashMapWithExpectedSize(Location.values().length);
        for(Location loc : Location.values()) {
            countMap.put(loc, 0);
        }
        return countMap;
    }
}
