package hadoopbook.ch5;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

abstract public class FunctionalMapperReducer<K1, V1, K2, V2, K3, V3> {

    abstract MRFunction<K1, V1, K2, V2> mapper();

    abstract MRFunction<K2, Iterable<V2>, K3, V3> reducer();

    final public Mapper<K1, V1, K2, V2> createMapper() {
        final MRFunction<K1, V1, K2, V2> f = mapper();
        return new Mapper<K1, V1, K2, V2>() {
            @Override
            protected void map(K1 key, V1 value, Context context) throws IOException, InterruptedException {
                Result<K2, V2> result = f.apply(key, value);
                if(!result.skip()) {
                    context.write(result.key(), result.value());
                }
            }
        };
    }

    final public Reducer<K2, V2, K3, V3> createReducer() {
        final MRFunction<K2, Iterable<V2>, K3, V3> f = reducer();
        return new Reducer<K2, V2, K3, V3>() {
            @Override
            protected void reduce(K2 key, Iterable<V2> values, Context context) throws IOException, InterruptedException {
                Result<K3, V3> result = f.apply(key, values);
                context.write(result.key(), result.value());
            }
        };
    }

    public final Result RESULT_SKIP = new Result(null, null);
    public static class Result<A, B> {

        private final A key;
        private final B value;

        public Result(A key, B value) {
            this.key = key;
            this.value = value;
        }

        public A key() {
            return key;
        }

        public B value() {
            return value;
        }

        public boolean skip() {
            return key == null && value == null;
        }

        @Override
        public boolean equals(Object o) {
            if(!(o instanceof Result)) {
                return false;
            }
            Result other = (Result) o;
            return key().equals(other.key()) &&
                    value().equals(other.value());
        }

        @Override
        public int hashCode() {
            return key().hashCode() * 163 + value().hashCode();
        }
    }

    public interface MRFunction<IN1, IN2, OUT1, OUT2> {
        public Result<OUT1, OUT2> apply(IN1 key, IN2 value);
    }
}
