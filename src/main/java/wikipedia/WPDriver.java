package wikipedia;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.File;

public class WPDriver extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
        if(args.length != 2) {
            System.err.println("Usage: [generic options] <input> <output>");
            ToolRunner.printGenericCommandUsage(System.err);
            System.exit(-1);
        }

        File output = new File(args[1]);
        if(output.exists()) {
            FileUtils.deleteDirectory(output);
        }

        Job job = new Job(getConf());
        job.setJarByClass(this.getClass());

        job.setInputFormatClass(WikipediaXmlInputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setMapperClass(WPPageMapper.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int exit = ToolRunner.run(new WPDriver(), args);
        System.exit(exit);
    }
}
