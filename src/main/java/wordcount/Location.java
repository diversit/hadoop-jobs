package wordcount;

public enum Location {
    TITLE(1),
    SUMMARY(2),
    TEXT(3);

    private final byte value;

    Location(int value) {
        this.value = (byte)value;
    }

    public static Location deserialize(byte value) {
        for(Location loc : Location.values()) {
            if(loc.value == value) return loc;
        }
        return null;
    }

    public byte serialize() {
        return value;
    }
}
