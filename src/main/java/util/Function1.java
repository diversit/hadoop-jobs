package util;

/**
 * Created with IntelliJ IDEA.
 * User: joostdenboer
 * Date: 5/11/12
 * Time: 1:33 AM
 * To change this template use File | Settings | File Templates.
 */
public interface Function1<A, B> {
    B apply(A a);
}
