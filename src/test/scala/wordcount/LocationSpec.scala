package wordcount

import org.scalatest.FeatureSpec
import org.scalatest.matchers.ShouldMatchers
import org.apache.hadoop.io.{Writable, WritableComparator}
import java.io._

class LocationSpec extends FeatureSpec with ShouldMatchers {

  feature("Serialize and deserialize Location") {
    scenario("Serialization/Deserialization roundtrip should result in same Location") {
      Location.values() foreach { loc => Location.deserialize(loc.serialize()) should be ===(loc) }
    }

    scenario("Deserialization/Serialization roundtrip should result in same byte") {
      Location.values() foreach { loc => Location.deserialize(loc.serialize()).serialize should be ===(loc.serialize) }
    }
  }

  feature("Write and read LocationCounter") {
    scenario("Write and read should result in same LocationCounter values") {
      val lc1 = new LocationCounter(Location.TEXT, 10);
      val lc2 = new LocationCounter()

      val pipeOut = new PipedOutputStream()
      val pipeIn = new PipedInputStream(pipeOut)
      val dataOutput = new DataOutputStream(pipeOut)
      val dataInput  = new DataInputStream(pipeIn)
      lc1.write(dataOutput)
      lc2.readFields(dataInput)

      lc1.getLocation should be ===(lc2.getLocation)
      lc1.getCount should be ===(lc2.getCount)
    }
  }

  feature("Comparing LocationCounter") {
    scenario("via Comparable") {
      val lc1 = new LocationCounter(Location.TEXT, 10)
      val lc2 = new LocationCounter(Location.TEXT, 11)

      val comp = WritableComparator.get(classOf[LocationCounter])
      comp.compare(lc1, lc2) should be <(0)
    }

    scenario("via RawComparator") {
      val lc1 = new LocationCounter(Location.TEXT, 10)
      val lc2 = new LocationCounter(Location.TEXT, 11)
      val slc1 = serialize(lc1)
      val slc2 = serialize(lc2)

      val comp = WritableComparator.get(classOf[LocationCounter])
      comp.compare(slc1, 0, slc1.length, slc2, 0, slc2.length) should be <(0)
    }
  }

  def serialize(writable: Writable) = {
    val out = new ByteArrayOutputStream
    val dataOut = new DataOutputStream(out)
    writable.write(dataOut)
    dataOut.close
    out.toByteArray
  }

  def deserialize(writable: Writable, bytes: Byte*) = {
    val in = new ByteArrayInputStream(bytes.toArray)
    val dataIn = new DataInputStream(in)
    writable.readFields(dataIn)
    dataIn.close
  }
}
