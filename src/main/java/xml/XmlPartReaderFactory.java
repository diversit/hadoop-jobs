package xml;

import java.io.IOException;
import java.io.InputStream;

public interface XmlPartReaderFactory {

    public XmlPartReader readerFor(String tag, InputStream inputStream, long startPosition) throws IOException;
}
