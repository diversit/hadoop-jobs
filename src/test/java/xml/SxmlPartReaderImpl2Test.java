package xml;

import java.io.IOException;
import java.io.InputStream;

public class SxmlPartReaderImpl2Test extends XmlPartReaderTestBase {

    @Override
    XmlPartReader createReaderWith(String tagName, InputStream inputStream, long startPosition) throws IOException {
        return new SxmlPartReaderImpl2(tagName, inputStream, startPosition);
    }
}
