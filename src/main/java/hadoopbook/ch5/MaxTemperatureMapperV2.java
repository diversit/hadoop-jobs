package hadoopbook.ch5;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MaxTemperatureMapperV2 extends Mapper<LongWritable, Text, Text, IntWritable> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // sample input:
        // 0067011990999991950051507004+68750+023550FM-12+038299999V0203301N00671220001CN9999999N9+01231+99999999999
        //                ^^^^ (year)                                                 (temperature) ^^^

        TemperatureParser parser = new TemperatureParser(value.toString());
        if(!parser.isTemperatureMissing()) {
            context.write(new Text(parser.getYear()), new IntWritable(parser.getTemperature()));
        }
    }
}
