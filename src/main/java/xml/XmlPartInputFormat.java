package xml;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import wikipedia.WikipediaXmlRecorderReader2;

import java.io.IOException;

public class XmlPartInputFormat extends FileInputFormat<Text, Text> {

    @Override
    public RecordReader<Text, Text> createRecordReader(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        RecordReader<Text, Text> recordReader = new XmlPartRecorderReader(new XmlPartReaderFactoryImpl());
//        recordReader.initialize(inputSplit, taskAttemptContext);
        return recordReader;
    }
}
