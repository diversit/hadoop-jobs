package xml


object Antixml extends App {

  val xml = <persons>
      <person>
        <name>Klaas</name>
        <age>43</age>
      </person>
      <person>
        <name>Piet</name>
        <age>33</age>
      </person>
    </persons>

  val nodes = xml \\ "name"
  println(nodes)
}
