package wikipedia;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.xml.sax.InputSource;
import wordcount.Location;
import wordcount.LocationCounter;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static wordcount.Location.TITLE;

public class WordCounterMapper extends Mapper<Text, Text, Text, LocationCounter> {

    private static XPath xpath = XPathFactory.newInstance().newXPath();
    public static final LocationCounter TITLE_COUNT_ONE = new LocationCounter(TITLE, 1);
    public static final LocationCounter TEXT_COUNT_ONE = new LocationCounter(Location.TEXT, 1);

    private Text word = new Text();
    public static final Pattern WORD_PATTERN = Pattern.compile("\\b[a-zA-Z]+\\b");

    @Override
    protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {

        try {
            String title = (String) xpath.evaluate(".//title", inputSourceForValue(value), XPathConstants.STRING);
            countWordsIn(title, context, TITLE_COUNT_ONE);

            String text  = (String) xpath.evaluate(".//text", inputSourceForValue(value), XPathConstants.STRING);
            countWordsIn(text, context, TEXT_COUNT_ONE);
        } catch (XPathExpressionException e) {
            throw new IOException(e);
        }
    }

    private void countWordsIn(String title, Context context, LocationCounter counter) throws IOException, InterruptedException {
        Matcher matcher = WORD_PATTERN.matcher(title);
        while (matcher.find()) {
            String sword = matcher.group();
            word.set(sword);
            context.write(word, counter);
        }
    }

    private InputSource inputSourceForValue(Text value) {
        return new InputSource(new StringReader(value.toString()));
    }
}
