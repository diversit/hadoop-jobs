package wikipedia;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;

public class WPPageMapper extends Mapper<Text, Text, Text, Text> {

    @Override
    protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {

        System.out.println("key: "+key.toString());
//        System.out.println("value: " + value.toString());

        XPath xpath = XPathFactory.newInstance().newXPath();
        try {
            String title = (String) xpath.evaluate(".//title", inputSourceForValue(value), XPathConstants.STRING);
            System.out.println("title: "+title);
            String text  = (String) xpath.evaluate(".//timestamp", inputSourceForValue(value), XPathConstants.STRING);
            context.write(new Text(title), new Text(text));
        } catch (XPathExpressionException e) {
            throw new IOException(e);
        }
    }

    private InputSource inputSourceForValue(Text value) {
        return new InputSource(new StringReader(value.toString()));
    }
}
