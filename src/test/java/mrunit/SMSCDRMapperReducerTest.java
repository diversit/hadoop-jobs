package mrunit;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static mrunit.SMSCDRMapper.CDRCounter.NONSMS;
import static org.apache.hadoop.thirdparty.guava.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SMSCDRMapperReducerTest {

    MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
    MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

    @Before
    public void setupMapperAndReducerAndDriver() {
        SMSCDRMapper mapper = new SMSCDRMapper();
        SMSCDRReducer reducer = new SMSCDRReducer();
        mapDriver = new MapDriver<LongWritable, Text, Text, IntWritable>();
        mapDriver.setMapper(mapper);
        reduceDriver = new ReduceDriver<Text, IntWritable, Text, IntWritable>();
        reduceDriver.setReducer(reducer);
        mapReduceDriver = new MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, IntWritable>();
        mapReduceDriver.setMapper(mapper);
        mapReduceDriver.setReducer(reducer);
    }

    @Test
    public void testMapper() {
        mapDriver.withInput(new LongWritable(), new Text("655209;1;796764372490213;804422938115889;6"));
        mapDriver.withOutput(new Text("6"), new IntWritable(1));
        mapDriver.runTest();
        assertThat(0L, is(mapDriver.getCounters().findCounter(NONSMS).getValue()));
    }

    @Test
    public void testReducer() {
        List<IntWritable> values = newArrayList(new IntWritable(1), new IntWritable(1));
        reduceDriver.withInput(new Text("6"), values);
        reduceDriver.withOutput(new Text("6"), new IntWritable(2));
        reduceDriver.runTest();
    }

    @Test
    public void testNonSmsCounter() {
        mapDriver.withInput(new LongWritable(), new Text("655209;0;796764372490213;804422938115889;6"));
        mapDriver.runTest();
        assertThat(1L, is(mapDriver.getCounters().findCounter(NONSMS).getValue()));
    }

    @Test
    public void testMapperReducer() {
        mapReduceDriver.withInput(new LongWritable(), new Text("655209;1;796764372490213;804422938115889;6"));
        mapReduceDriver.withInput(new LongWritable(), new Text("655209;0;796764372490213;804422938115889;6"));
        mapReduceDriver.withInput(new LongWritable(), new Text("655209;1;796764372490213;804422938115889;6"));
        mapReduceDriver.withInput(new LongWritable(), new Text("655209;1;796764372490213;804422938115889;2"));
        mapReduceDriver.withOutput(new Text("2"), new IntWritable(1));
        mapReduceDriver.withOutput(new Text("6"), new IntWritable(2));
        mapReduceDriver.runTest();
        assertThat(1L, is(mapReduceDriver.getCounters().findCounter(NONSMS).getValue()));
    }
}
