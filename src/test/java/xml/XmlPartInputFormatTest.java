package xml;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.RecordReader;
import org.junit.Test;
import wikipedia.WikipediaXmlInputFormat;
import wikipedia.WikipediaXmlRecorderReader2;
import wikipedia.WikipediaXmlRecorderReaderTestBase;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class XmlPartInputFormatTest extends WikipediaXmlRecorderReaderTestBase {

    @Test
    public void shouldReturnXmlPartRecordReader() throws Exception {
        setupRecordReaderParams(0, 1000, TITLE_TAG);
        XmlPartInputFormat inputFormat = new XmlPartInputFormat();
        RecordReader<Text,Text> recordReader = inputFormat.createRecordReader(fileSplit, context);
        assertThat(recordReader instanceof XmlPartRecorderReader, is(true));
    }
}
