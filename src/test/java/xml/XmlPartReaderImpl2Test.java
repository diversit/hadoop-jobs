package xml;

import java.io.IOException;
import java.io.InputStream;

public class XmlPartReaderImpl2Test extends XmlPartReaderTestBase {

    @Override
    XmlPartReader createReaderWith(String tagName, InputStream inputStream, long startPosition) throws IOException {
        return new XmlPartReaderImpl2(tagName, inputStream, startPosition);
    }
}
