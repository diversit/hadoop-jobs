package util;

import java.util.List;

import static org.apache.hadoop.thirdparty.guava.common.collect.Lists.newArrayList;

public class Collections3 {

    public static <A,B> B foldLeft(final Iterable<A> list, final B b, final Function2<A,B> f) {
        B result = b;
        for(A a : list) {
            result = f.apply(result, a);
        }
        return result;
    }

    public static <A,B> List<B> map(final Iterable<A> list, final Function1<A, B> f) {
        List<B> result = newArrayList();
        result = foldLeft(list, result, new Function2<A, List<B>>() {
            @Override
            public List<B> apply(List<B> bs, A a) {
                bs.add(f.apply(a));
                return bs;
            }
        });
        return result;
    }

    public static <A> List<A> filter(final Iterable<A> list, final Function1<A, Boolean> f) {
        List<A> result = newArrayList();
        result = foldLeft(list, result, new Function2<A, List<A>>() {
            @Override
            public List<A> apply(List<A> as, A a) {
                if(f.apply(a)) {
                    as.add(a);
                }
                return as;
            }
        });
        return result;
    }
}
