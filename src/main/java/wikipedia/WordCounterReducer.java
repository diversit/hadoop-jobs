package wikipedia;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.thirdparty.guava.common.collect.Maps;
import wordcount.Location;
import wordcount.LocationCounter;

import java.io.IOException;
import java.util.Map;

public class WordCounterReducer extends Reducer<Text, LocationCounter, Text, Map<Location, Integer>> {

    @Override
    protected void reduce(Text key, Iterable<LocationCounter> values, Context context) throws IOException, InterruptedException {
        Map<Location, Integer> countMap = createNewCountMap();

        for(LocationCounter lc : values) {
            incrementCountForLocation(countMap, lc);
        }

        context.write(key, countMap);
    }

    private void incrementCountForLocation(Map<Location, Integer> countMap, LocationCounter lc) {
        Integer count = countMap.get(lc.getLocation());
        countMap.put(lc.getLocation(), count + lc.getCount());
    }

    private Map<Location, Integer> createNewCountMap() {
        Map<Location, Integer> countMap = Maps.newHashMapWithExpectedSize(Location.values().length);
        for(Location loc : Location.values()) {
            countMap.put(loc, 0);
        }
        return countMap;
    }
}
