package xml;

import java.io.IOException;
import java.io.InputStream;

public class XmlPartReaderImplTest extends XmlPartReaderTestBase {

    @Override
    XmlPartReader createReaderWith(String tagName, InputStream inputStream, long startPosition) throws IOException {
        return XmlPartReaderImpl.readerFor(tagName, inputStream, startPosition);
    }
}
