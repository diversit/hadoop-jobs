package xml;

import org.apache.hadoop.thirdparty.guava.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CompareXmlPartReaders {

    public static final String TAG = "title";
    String wikiFile = "/Users/joostdenboer/Downloads/nlwiki-latest-pages-articles.xml";
//    File wikiFile = new File("/Users/joostdenboer/Downloads/nlwiki-latest-pages-articles-short.xml");

    private List<XmlPartReader> readers;

    @Before
    public void createReadersForTest() throws IOException {
        readers = Lists.newArrayList(
//            XmlPartReaderImpl.readerFor(TAG, new FileInputStream(wikiFile), 0),
            new SxmlPartReaderImpl2(TAG, new FileInputStream(wikiFile), 0L),
            new XmlPartReaderImpl2(TAG, new FileInputStream(wikiFile), 0)
        );
    }


    @Test
    public void compareReaders() throws Exception {
        for(XmlPartReader reader : readers) {
            measureReader(reader);
        }
    }

    private void measureReader(XmlPartReader reader) {

        long start = new Date().getTime();
        long partsCount = 0;
        for(String part : reader) {
            partsCount++;
        }
        long end = new Date().getTime();

        assertThat(partsCount > 0, is(true));
        System.out.printf("%s:Time to read %d titles in %s: %d milliseconds\n", reader.getClass().getName(), partsCount, wikiFile, end - start);
    }
}
