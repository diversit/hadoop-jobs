package mrunit;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class SMSCDRMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    public static final IntWritable ONE = new IntWritable(1);
    public static final int STATUS_CODE = 4;
    public static final int CDR_TYPE = 1;
    public static final String SMS_TYPE = "1";

    static enum CDRCounter {
        NONSMS;
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        // sample input:
        // CDRID:CDRType:Phone1;Phone2;SMS Status Code
        // 655209;1;796764372490213;804422938115889;6

        String[] elements = value.toString().split(";");
        if(SMS_TYPE.equals(elements[CDR_TYPE])) {
            context.write(new Text(elements[STATUS_CODE]), ONE);
        } else {
            context.getCounter(CDRCounter.NONSMS).increment(1);
        }
    }
}
