package wikipedia;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import wordcount.LocationCounter;
import xml.XmlPartInputFormat;

import java.io.File;

public class WordCounterDriver extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
        if(args.length != 2) {
            System.err.printf("Usage: %s [generic options] <input> <output>", getClass().getSimpleName());
            ToolRunner.printGenericCommandUsage(System.err);
            return -1;
        }

        String output = args[1];
        if(new File(output).exists()) {
            FileUtils.deleteDirectory(new File(args[1]));
        }

        Job job = new Job(getConf(), "Wikipedia Word Counter");
        job.setJarByClass(getClass());
//        job.setInputFormatClass(WikipediaXmlInputFormat.class);
        job.setInputFormatClass(XmlPartInputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setMapperClass(WordCounterMapper.class);
//        job.setCombinerClass(WordCounterReducer.class);
        job.setReducerClass(WordCounterReducer2.class);

        job.setOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LocationCounter.class);
//        job.setOutputValueClass(IntWritable.class);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception {
        int exit = ToolRunner.run(new WordCounterDriver(), args);
        System.exit(exit);
    }
}
